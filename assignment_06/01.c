#include <stdio.h>
 void fun1(int n);
 
int main()
{
	int n;
	printf("Enter how many lines you want to print : ");
	scanf("%d",&n);
	printf("\n............Pattern 1...........\n\n");
	fun1(n);
	printf("\n");
	return 0;

}

void fun1(int n)
{
	int i;
	if(n==0)
	return;
	else
	{
		for(i=1;i<=n;i++)
		printf("%d",i);
		printf("\n");
	}
}
