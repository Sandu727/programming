#include <stdio.h>


int people(int price);
float cost(int people);
float profit(int people,int price);
float netprofit(float prf,float cst);

int main(){
	int price;
	int p;
	float c,pr,net;
	do{
		printf("\nEnter ticket price : ");
		scanf("%d",&price);
		p=people(price);
		c=cost(p);
		pr=profit(p,price);
		net=netprofit(pr,c);
		printf("\nProfit : %.2f",net);
	}while(price!=0);
	return 0;
}

int people(int price){
	int a = 120+(15-price)*4;
	return a;
}

float cost(int people){
	float cost = 300+people*3;
	return cost;
}

float profit(int people,int price){
	float pro = people*price;
	return pro;
}

float netprofit(float pr,float c){
	float net = pr-c;
	return net;
}
